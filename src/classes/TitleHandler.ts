import { Application } from "./Application";

import { Observable, zip, concat, of, pipe, from } from "rxjs";
import { delay, mergeMap, toArray, flatMap, pluck, switchMap, map, last, distinctUntilChanged,
        mergeScan, take, scan, concatMap, tap, filter, mergeAll, combineLatest } from "rxjs/operators";


export class TitleHandler {
    owner:Application;
    constructor( owner:any ){
        if( !( owner instanceof Application ) ) throw "Invalid owner class passed to TitleHandler constructor. Must be of type Application."

        this.owner = owner;
    };

    /*
     * Assembles the filters for the title handler. Does so by combining the remove and replace
     * filters specified in the provided application's configuration file.
     *
     * Filters that are disabled inside the configuration are filtered out before the stream
     * is returned from the function.
     *
     */
    assembleFilters() : Observable<string[]> {
        const config = this.owner.getConfig();

        // Create a stream of the remove filters; mapping each to a replace filter of ''
        const removeFilters:Observable<string[]> = of( config ).pipe(
            pluck( "filters", "remove" ),
            map( x => [ "\\b(?:" + x.join("|") + ")\\b", "", "REMOVE" ] )
        );

        // Create a stream of the replace filters, filtering out any filters that are disabled inside
        // the applications configuration file.
        const replaceFilters:Observable<any> = of( config ).pipe(
            pluck( "filters", "replace" ),
            flatMap( p => p ),
            map( p => {
                p[2] = p[2] || 'UNNAMED';
                return p
            })
        )

        // Return a stream of the filters, concatting the remove filters and the replace filters together.
        return concat( removeFilters, replaceFilters ).pipe(
            toArray(),
            flatMap( x => x ),
            filter( x => config.settings.filter[x[2]] )
        );
    };


    /*
     * Uses a stream of filters provided from 'assembleFilters' above to filter content out of the
     * items provided.
     *
     * Items should be an array as it is converted to an observable before processing begins.
     *
     * Outputs an array of the output files in a stream. Uses toArray and last to provide only one
     * version of the output as an array
     */
    filterItems( items:string[] ) : Observable<string[]> {
        const filterStream = this.assembleFilters();
        const itemStream = from( items );

        const _apply = title => filterStream.pipe(
            map( filter => title.replace( new RegExp( filter[0], "gi" ), filter[1] ) ),
            tap( out => title = out )
        )

        const _applyWithCallback = title => {
            // TODO
        }

        const applyFilter = item =>
            of( item ).pipe( concatMap( title => _apply( title ) ), last() );

        return itemStream.pipe(
            concatMap( item => applyFilter( item ) ),
            toArray()
        );
    }
};
